I. This example demonstrates how to collect code coverage by integration tests, tests are located in a separate Java Maven module.
SonarQube aggregates code coverage by unit tests and integration tests to compute an overall code coverage.

Prerequisites
=============
* [SonarQube](http://www.sonarqube.org/downloads/) 5.6+
* Maven 3+

Usage
=====
* Rename the other pom.xml as something else, and rename the pom.xml.sonarqube to pom.xml
* Build the project, execute all the tests and analyze the project with SonarQube Scanner for Maven:

        mvn clean install sonar:sonar

II. Below is Demo for NEXUS

Prerequisites
=============
* [Nexus Server](https://www.intertech.com/Blog/lightning-tutorial-install-nexus/) 5.6+
    [Or](https://medium.com/@baymac/nexus-repository-manager-setup-for-jenkins-plugin-development-f6246dc669b8) Latest in VM
* Maven 3+

Notice that this project consists of 3 sub-projects, namely:
* app-java
* app-it
* app-groovy

Ensure that all these 3 projects and the parent project pom.xml contains
```
    <distributionManagement>
        <repository>
            <id>nexus-releases</id>
            <name>maven-releases</name>
            <url>http://<ip>:8081/nexus/repository/maven-bpi-releases/</url>
        </repository>
        <snapshotRepository>
            <id>nexus-snapshots</id>
            <name>maven-snapshots</name>
            <url>http://<ip>:8081/nexus/repository/maven-bpi-snapshot/</url>
        </snapshotRepository>
    </distributionManagement>

    <repositories>
        <repository>
            <id>nexus</id>
            <url>http://<ips>:8081/nexus/repository/maven-bpi-group/</url>
        </repository>
    </repositories>
```

Ensure that the home folder where you're running the maven, the settings.xml contains
```
?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">

  <servers>
    <server>
      <id>nexus-snapshots</id>
      <username>admin</username>
      <password>admin123</password>
    </server>
    <server>
      <id>nexus-releases</id>
      <username>admin</username>
      <password>admin123</password>
    </server>
  </servers>

  <mirrors>
    <mirror>
      <id>central</id>
      <name>central</name>
      <url>http://35.193.165.103:8081/nexus/repository/maven-bpi-group/</url>
      <mirrorOf>*</mirrorOf>
    </mirror>
  </mirrors>

  <profiles>
    <profile>
    	<id>nexus</id>
    	<!--Enable snapshots for the built in central repo to direct -->
    	<!--all requests to nexus via the mirror -->
    	<repositories>
        	<repository>
        		<id>central</id>
        		<url>http://central</url>
        		<releases><enabled>true</enabled></releases>
        		<snapshots><enabled>true</enabled></snapshots>
        	</repository>
    	</repositories>
    	<pluginRepositories>
        	<pluginRepository>
        		<id>central</id>
        		<url>http://central</url>
        		<releases><enabled>true</enabled></releases>
        		<snapshots><enabled>true</enabled></snapshots>
        	</pluginRepository>
    	</pluginRepositories>
    </profile>
  </profiles>
  <activeProfiles>
    <!--make the profile active all the time -->
    <activeProfile>nexus</activeProfile>
  </activeProfiles>

</settings>
```

Then invoke `mvn clean install package deploy`